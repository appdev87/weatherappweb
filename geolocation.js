(function () {
    let positionOptions = {
        enableAccuracy: true,
        timeout: 10000,
        maximumAge: 0
    }


    let watchID = navigator.geolocation.watchPosition(
        geolocationSuccessCallback,
        geolocationFailureCallback,
        positionOptions
    );


    function geolocationSuccessCallback(position) {

        let lat = position.coords.latitude;
        let lon = position.coords.longitude;

        let storage = window.localStorage;
        storage.setItem('lat', lat);
        storage.setItem('lon', lon);

        // $('#latitude').value(parseFloat(lat).toFixed(6));
        // $('#longitude').val(parseFloat(lon).toFixed(6));
    }

    function geolocationFailureCallback(error) {
        alert("Error in accessing your current location with error: " + error);
    }

    function stopWatch() {
        navigator.geolocation.clearWatch(watchID);
        return watchID;
    }

    // Sets the timeOut to launch after 1 sec of polling parsing the stopWatch function
    setTimeout(stopWatch, 1000);
})();
