﻿/*jshint esversion: 6 */
$(function () {

// ********************* Line chart ******************
    let LineChart = (function () {
        /**
         * Class LineChart to separate the different part of how the data is display
         *
         * @constructor calls the weather class to get access to all its functions
         */
        function LineChart() {
            let canvas = document.getElementById('theCanvas');
            let ctx = canvas.getContext('2d');

            const circleCalc = 2 * Math.PI;

            this.getCircleCalc = function () {
                return circleCalc;
            };

            this.getContext = function () {
                return ctx;
            };
        }

        /**
         * Function to draw the background and layout for the line chart. It uses the variable for the day found in
         * the WeatherData call, namely fillNextDays() anonymous function.
         * When the circles are drawn, a total of 260 is added until the next line. Dividing it by 4 to get
         * where the circle is drawn, the 65 is returned which is the amount that is added to get the exact time,
         * of where to draw the dayLine.
         */
        LineChart.prototype.setUpChart = function () {
            console.log('Time period of the day: ' + this.timeDict.timeOfDay);
            let dataTime = this.timeDict.timeOfDay;
            let xAxisChart = 60 +(dataTime+65);
            let yAxisChart = 550 / 4;
            let degrees = 40;

            // Loop used for displaying the vertical lines and the days
            for (let i = 1; i < 5; i++) {
                this.getContext().strokeStyle = 'rgba(142, 68, 173,0.3)';
                this.getContext().beginPath();
                this.getContext().moveTo(xAxisChart, 0);
                this.getContext().lineTo(xAxisChart, 550);
                this.getContext().stroke();

                let day = this.fillNextDays('long', i);
                let currentDay = day.slice(0, day.indexOf(' '));
                // Plus 15 to get some extra padding between the line and the text
                this.getContext().fillText(currentDay, xAxisChart + 15, 15);
                xAxisChart += 200;
            }

            // Loop used for displaying the horizontal lines and the degrees
            for (let i = 0; i < 5; i++) {
                this.getContext().strokeStyle = 'rgba(142, 68, 173,0.3)';
                this.getContext().beginPath();
                this.getContext().moveTo(0, yAxisChart);
                this.getContext().lineTo(1050, yAxisChart);
                this.getContext().stroke();
                this.getContext().fillText(`${degrees}°`, 10, yAxisChart - 10);
                yAxisChart += 105;
                degrees -= 10;
            }
        };


        LineChart.prototype.drawCircle = function (x, y) {

            this.getContext().fillStyle = 'blue';
            this.getContext().beginPath();
            this.getContext().arc(x, y, 5, 0, this.getCircleCalc());
            this.getContext().fill();
            this.getContext().stroke();
        };

        LineChart.prototype.drawLine = function (x1, y1, x2, y2) {
            this.getContext().strokeStyle = 'blue';
            this.getContext().lineWidth = 2;
            this.getContext().beginPath();
            this.getContext().moveTo(x1, y1);
            this.getContext().lineTo(x2, y2);
            this.getContext().stroke();
        };

        return LineChart;
    })();

//  ********************* WEATHER CLASS STARTS HERE  *********************

    let WeatherData = (function (parent) {
        WeatherData.prototype = new LineChart();
        WeatherData.prototype.constructor = WeatherData;

        function WeatherData() {
            parent.call(this);
        }


        WeatherData.prototype.getWeatherInfo = function (lat, lon) {
            console.log(lat, lon);
            
            document.getElementById('latitude').value = parseFloat(lat).toFixed(6);
            document.getElementById('longitude').value = parseFloat(lon).toFixed(6);

            const apiKey = 'b56ba3dc4b23ead42e2237a4d0f1f5bd';
            const url =
                'http://api.openweathermap.org/data/2.5/forecast?lat=' + lat + '&lon=' + lon + '&units=metric&appid=' + apiKey;


            const request = new XMLHttpRequest();
            request.onreadystatechange = function () {
                if (request.readyState === 4 && request.status === 200) {
                    weatherData.populateTable(this.response);
                }
            };

            try {
                request.open("GET", url);
                
                request.send();
            } catch (ex) {
                alert('Exception of type ' + ex.name + ' occurred: ' + ex.message + '.');
            }
        };


        WeatherData.prototype.populateTable = function (response) {
            let weatherResponse;
            try {
                weatherResponse = JSON.parse(response);
            } catch (e) {
                console.log(e);
            }

            // Fill in 1st Day
            let weatherDate = weatherResponse.list[0].dt;
            let date = new Date(weatherDate * 1000);
            let fullDate = weatherData.processDay(weatherResponse, date);
            $(`#day${1}`).text(fullDate);

            // Fill in other days
            for (let i = 1; i < 5; i++) {
                let day = weatherData.fillNextDays('short', i);
                $(`#day${i + 1}`).text(day);
            }
        };

        WeatherData.prototype.dateArr = {};
        WeatherData.prototype.counter = 0;
        WeatherData.prototype.processDay = function (weatherResponse, date) {

            let dateOptions = {day: 'numeric', weekday: 'short', year: 'numeric', month: 'short'};
            let fullDate = new Intl.DateTimeFormat('en-AU', dateOptions).format(date);
            fullDate = fullDate.replace(',', '');
            fullDate = fullDate.split(' ');
            weatherData.dateArr[weatherData.counter] = fullDate[2];
            weatherData.counter++;
            fullDate = fullDate[0] + ' ' + fullDate[2] + ' ' + fullDate[1] + ' ' + fullDate[3];
            weatherData.fillTable(weatherResponse);

            return fullDate;
        };

        WeatherData.prototype.fillTable = function (weatherResponse) {
            let reports = weatherResponse.list;
            let weatherPos = 0;
            let timePassed = 0;
            let temperatureDict = [];

            for (let i = 0; i < reports.length; i++) {

                let weatherDate = weatherResponse.list[i].dt;
                let time = weatherData.processTime(weatherDate);
                $(`#forecast${time.timeOfDay}`).text(time.time);

                let temperature1 = Math.ceil(parseInt(reports[weatherPos].main.temp));

                weatherPos++;

                // Plus 25 to adjust the display of the dot
                timePassed += (time.timeOfDay + 25);

                this.drawCircle(timePassed, 550 - (temperature1*10));
                temperatureDict[i] = [timePassed, 550 - (temperature1*10)];

            }

            for (let i = 0, j = 1; i < reports.length-1; i++, j++) {

                try {

                    if (temperatureDict[j] === 'undefined' || temperatureDict[i] === 'undefined') {
                        j = 39;
                        i = 39;
                        this.drawLine(temperatureDict[i][0], temperatureDict[i][1],
                            temperatureDict[j][0], temperatureDict[j][1]);
                    } else {
                        this.drawLine(temperatureDict[i][0], temperatureDict[i][1],
                            temperatureDict[j][0], temperatureDict[j][1]);
                    }
                } catch (err) {
                    console.log(err);
                }
            }

            weatherData.populateTableWithWeatherData(weatherResponse, weatherData.timeDict.timeOfDay);

        };

        WeatherData.prototype.populateTableWithWeatherData = function (weatherResponse, startPos) {
            let tableNodes = document.getElementsByTagName("td");

            let reports = weatherResponse.list;
            let weatherPos = 0;
            for (let i = startPos; i < reports.length; i++) {

                const temperature = Math.ceil(parseInt(reports[weatherPos].main.temp));
                const icon = reports[weatherPos].weather[0].icon;
                const description = reports[weatherPos].weather[0].description;

                const currentWeather = weatherData.produceWeatherIcon(icon, description);

                tableNodes[i].innerHTML += `<h4> ${temperature}&deg; </h4>`;
                tableNodes[i].innerHTML += currentWeather;
                tableNodes[i].innerHTML += `<h6>${description}</h6>`;
                weatherPos++;
            }


            // Process the weather reports for each city
            for (let i = 8; i > startPos; i--) {
                // Prepare the output
                document.getElementById(`weather5${i}`).innerHTML = '';
            }

            // Set-up the lineChart
            this.setUpChart();
        };

        WeatherData.prototype.produceWeatherIcon = function (weatherData, description) {
            return '<p><img src="res/images/' + weatherData + '.png" alt="' + weatherData +
                '" title="' + description + '"/> </p>';
        };


        WeatherData.prototype.fillNextDays = function (dayOutputType, days) {

            // Format date using the DateTimeFormat MDN Documentation
            let currentDate = new Date();
            currentDate = currentDate.setDate(new Date().getDate() + days);

            let date = new Date(currentDate);
            let weatherOptions = {weekday: dayOutputType, month: 'short'};
            let dayOfWeek = new Intl.DateTimeFormat('en-AU', weatherOptions).format(date);
            let monthDaySplice = dayOfWeek.split(' ');

            return monthDaySplice[1] + ' ' + monthDaySplice[0] + ' ' + date.getDate() + ' ' + date.getFullYear();
        };

        WeatherData.prototype.timeDict = {};
        WeatherData.prototype.processTime = function (weatherDateTime) {
            let dateTime = new Date(weatherDateTime * 1000).toLocaleString('en-AU');
            let stringifyJSONTime = JSON.stringify(dateTime);

            let amPM = stringifyJSONTime.slice(-3, -1);
            let currentTime = stringifyJSONTime.split(' ');
            currentTime = stringifyJSONTime.slice(12, currentTime[1][1] === '0' ? 15 : 14);
            let time = currentTime + ' ' + amPM;

            let posAMPM = (amPM === 'am') ? 1 : 5;
            switch (currentTime.trim()) {
                case '1':
                    break;

                case '4':
                    posAMPM = 1 + posAMPM;
                    break;

                case '7':
                    posAMPM = 2 + posAMPM;
                    break;

                case '10':
                    posAMPM = 3 + posAMPM;
                    break;
            }

            weatherData.timeDict.time = time;
            weatherData.timeDict.timeOfDay = posAMPM;

            return weatherData.timeDict;
        };

        return WeatherData;
    })(LineChart);

    var weatherData = new WeatherData();

    $('#submitBtn').click(function () {

        let storage = window.localStorage;
        let lat = storage.getItem('lat');
        let lon = storage.getItem('lon');

        if (validateInput(parseFloat(lat)) && validateInput(parseFloat(lon))) {
            $('.errorMsg').innerHTML = '';
            weatherData.getWeatherInfo(lat, lon);
        }

        // storage.clear();
    });

    /**
     * Function to test that no malicious code were entered using regex.test() function.
     * @see https://stackoverflow.com/questions/3518504/regular-expression-for-matching-latitude-longitude-coordinates
     *      for coordinates pattern matching.
     * @param coords the type of longitude and latitude that were parsed as parameters
     * @returns {boolean} if there was any error message, it test on the emptiness of a string
     */
    function validateInput(coords) {
        if (coords !== null) {
            let regex = /-?^(\d+(\.\d+)?)$/;
            let error = regex.test(coords) ? '' : 'Please enter valid Coordinates!';
            $('.errorMsg').html(error).css('background', 'none').css('color', 'red');
            return !error;

        }
    }
});

